package fr.orsys.sms.beans;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class MessageTwo {
	
	private List<String> receivers;
	
	private String message;
	
	private String priority;
	
	private boolean senderForResponse;

	public MessageTwo() {
		super();		
	}

	public MessageTwo(List<String> receivers, String message, String priority, boolean senderForResponse) {
		super();
		this.receivers = receivers;
		this.message = message;
		this.priority = priority;
		this.senderForResponse = senderForResponse;
	}	

	public List<String> getReceivers() {
		return receivers;
	}

	public void setReceivers(List<String> receivers) {
		this.receivers = receivers;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public boolean isSenderForResponse() {
		return senderForResponse;
	}

	public void setSenderForResponse(boolean senderForResponse) {
		this.senderForResponse = senderForResponse;
	}
	
	
	public  String messageJson(String receivers, String contenuDuMessage) throws JsonProcessingException {
		
		List<String> arrPhone = new ArrayList<>();
		arrPhone.add("+33" + receivers.substring(1));		
		
		  MessageTwo content = new MessageTwo(arrPhone, contenuDuMessage, "high", true);		  
          
          ObjectMapper mapper = new ObjectMapper();
          
          ObjectNode user = mapper.createObjectNode();
          
          user.putPOJO("receivers", content.getReceivers());
          user.put("message", content.getMessage());
          user.put("priority", content.getPriority());
          user.put("senderForResponse", content.isSenderForResponse());           
                   
          return mapper.writeValueAsString(user);
	}
	
}
